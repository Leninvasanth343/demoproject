package com.example.demo.module.dto;

import lombok.Data;

@Data
public class UserDTO {

	private Integer id;

	private String lastName;

	private String firstName;

	private Integer age;

}
