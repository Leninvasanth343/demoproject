package com.example.demo.module.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.UserMaster;
import com.example.demo.module.dto.UserDTO;
import com.mysql.fabric.xmlrpc.base.Array;

@Repository
@Transactional
public class UserDao {

	@Autowired
	private Environment environment;

	@Autowired
	private SessionFactory sessionFactory;

	public UserDTO getUserdetails(Integer id) {
		// TODO Auto-generated method stub

		UserMaster userMaster = sessionFactory.getCurrentSession().get(UserMaster.class, id);

		if (userMaster == null) {
			return null;
		}

		UserDTO UserDTO = new UserDTO();
		UserDTO.setAge(userMaster.getAge());
		UserDTO.setFirstName(userMaster.getFirstName());
		UserDTO.setLastName(userMaster.getLastName());
		UserDTO.setId(userMaster.getId());

		return UserDTO;
	}

	public Integer newUser(UserDTO userDTO) {

		UserMaster userMaster = new UserMaster();
		userMaster.setAge(userDTO.getAge());
		userMaster.setFirstName(userDTO.getFirstName());
		userMaster.setLastName(userDTO.getLastName());
		sessionFactory.getCurrentSession().save(userMaster);

		return userMaster.getId();
	}

	public Boolean updateUser(UserDTO userDTO) {

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		try {

			UserMaster userMaster = session.get(UserMaster.class, userDTO.getId());
			userMaster.setAge(userDTO.getAge());
			userMaster.setFirstName(userDTO.getFirstName());
			userMaster.setLastName(userDTO.getLastName());
			session.update(userMaster);

			tx.commit();
			session.close();

		} catch (Exception e) {

			e.printStackTrace();
			tx.commit();
			session.close();
			return false;
		}

		return true;
	}

	public void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		try {
			String deleteQry = "DELETE FROM UserMaster WHERE id=:id ";
			Query<?> query = sessionFactory.getCurrentSession().createQuery(deleteQry);
			query.setParameter("id", id);
			query.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<UserDTO> getUserdetailsList() {
		// TODO Auto-generated method stub
		
		String deleteQry = "FROM UserMaster";
		Query<?> query = sessionFactory.getCurrentSession().createQuery(deleteQry);
		List<UserMaster> list=(List<UserMaster>)query.list();
		
		List<UserDTO> userList=new ArrayList<>();
		
		for (UserMaster userMaster : list) {
			UserDTO UserDTO = new UserDTO();
			UserDTO.setAge(userMaster.getAge());
			UserDTO.setFirstName(userMaster.getFirstName());
			UserDTO.setLastName(userMaster.getLastName());
			UserDTO.setId(userMaster.getId());
			userList.add(UserDTO);
		}
		return userList;
	}
}
