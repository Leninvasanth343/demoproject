package com.example.demo.module.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.module.dao.UserDao;
import com.example.demo.module.dto.UserDTO;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	
	@Autowired
	UserDao userDao;

	@Override
	public UserDTO getUserdetails(Integer id) {
		// TODO Auto-generated method stub
		return userDao.getUserdetails(id);
	}

	@Override
	public Integer newUser(UserDTO userDTO) {
		// TODO Auto-generated method stub
		return userDao.newUser(userDTO);
	}

	@Override
	public Boolean updateUser(UserDTO userDTO) {
		// TODO Auto-generated method stub
		return userDao.updateUser(userDTO);
	}

	@Override
	public  void deleteUser(Integer id) {
		// TODO Auto-generated method stub
		 userDao.deleteUser(id);
	}

	@Override
	public List<UserDTO> getUserdetailsList() {
		// TODO Auto-generated method stub
		return userDao.getUserdetailsList();
	}

}
