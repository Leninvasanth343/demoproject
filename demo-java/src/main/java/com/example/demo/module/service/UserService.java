package com.example.demo.module.service;

import java.util.List;

import com.example.demo.module.dto.UserDTO;

public interface UserService {

	UserDTO getUserdetails(Integer srNo);

	Integer newUser(UserDTO userDTO);

	Boolean updateUser(UserDTO userDTO);

	void deleteUser(Integer id);

	List<UserDTO> getUserdetailsList();

}
