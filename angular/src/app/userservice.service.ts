import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  constructor(private http: HttpClient) { }

  public userList(): Observable<any> {
    return this.http.get<any>(environment.baseurl+'/api/user/list');
  }

  public createuser(data): Observable<any> {
    return this.http.post<any>(environment.baseurl+'/api/user/create',data);
  }

  public deleteuser(id): Observable<any> {
    return this.http.delete<any>(environment.baseurl+'/api/user/delete?id=' + id);
  }

  
}
