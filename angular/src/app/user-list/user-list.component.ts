import { Component, OnInit } from '@angular/core';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users=[];

  constructor(private userService: UserserviceService) {

    this.getUserList();
   }

  ngOnInit(): void {

    this.getUserList();

    

      
  }

  delete(id){

    this.userService.deleteuser(id).subscribe(data =>{

      this.getUserList();
      });

     
  }

  getUserList(){
    this.userService.userList().subscribe(data =>{
      
      console.log(data);
      this.users=data;
      
  
      });
  }

}
