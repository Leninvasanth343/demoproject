import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  form:FormGroup; 
  constructor(private userService: UserserviceService,private router: Router) { 

    
  }

  ngOnInit(): void {

    

    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required)
      

      
     });
  }

  onSubmit(){
    this.userService.createuser(this.form.value).subscribe(data =>{
      
      console.log(data);
      
      
  
      });

    this.form.reset();
    this.router.navigateByUrl('/list');
  }
}
